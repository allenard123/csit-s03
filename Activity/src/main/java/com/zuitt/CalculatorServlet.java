package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


public class CalculatorServlet extends HttpServlet {


	private static final long serialVersionUID = 1L;

	
	public void init() throws ServletException{
		System.out.println("***************************************");
		System.out.println(" CalculatorServlet has been initialized ");
		System.out.println("***************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		System.out.println("Hello from the calculator app");
		String operator = req.getParameter("operator");
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int result = 0;
		
		switch(operator) {
		case "add":
            result = num1 + num2;
            break;
        case "multiply":
            result = num1 * num2;
            break;
        case "subtract":
            result = num1 - num2; 
            break;
        case "divide":
            result = num1 / num2;
            break;
            
		}
		
		PrintWriter out = res.getWriter();
		
		out.println("<p> The two numbers you provided are: "  + num1 + ", " + num2 +"<br><br>");
		out.println("The operation that you wanted is: " + operator +"<br><br>");
		out.println("The result is: " + result + "</p>");
}		
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the Calculator App</h1>");
		out.println("To use the app, input two numbers and operation.<br><br>");
		out.println("Hit the submit button after filling in the details.<br><br>");
		out.println("You will get the result shown in your browser!");
		
	}
	
	public void destroy() {
		System.out.println("***************************************");
		System.out.println(" CalculatorServlet has been destroyed ");
		System.out.println("***************************************");
	}
	
	
	
}
